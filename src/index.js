import '@babel/polyfill';
import 'url-polyfill';
import dva from 'dva';
import { message } from 'antd';


import createHistory from 'history/createHashHistory';
// user BrowserHistory
// import createHistory from 'history/createBrowserHistory';
import createLoading from 'dva-loading';
import 'moment/locale/zh-cn';
import './rollbar';

import './index.less';

const Console = console;

// 1. Initialize
const app = dva({
  history: createHistory(),
  onError(e, dispatch) {
    Console.log('全局错误捕获：', e);

    if (e.code === 209) {
      message.error('会话失效，请重新登录！');
      dispatch({ type: 'login/logout' });
    }
    if (e.code === 105) {
      message.error('格式不正确, 请导入指定要求的Excel！');
    }
    if (e.code === 500) {
      message.error(e.message);
    }
    // if (e.statusText === 'Payload Too Large') {
    //   message.error(`文件体积过大${fileVolumeLimit}M`);
    // }
  },
});

// 2. Plugins
app.use(createLoading());

// 3. Register global model
app.model(require('./models/global').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');

export default app._store; // eslint-disable-line
