import {message} from 'antd';
import { fakeRegister } from '../services/user';
import { setAuthority } from '../utils/authority';
import { reloadAuthorized } from '../utils/Authorized';

export default {
  namespace: 'register',

  state: {
    status: undefined,
  },

  effects: {
    *submit({ payload }, { call, put }) {
      try {
        const response = yield call(fakeRegister, payload);
        yield put({
          type: 'registerHandle',
          payload: {
            response,
            status: 'ok',
          },
        });
      } catch (error) {
        message.error(error.message);
      }
    },
    *resetStatus(_,{put}){
      yield put({
        type: 'save',
        payload: {
          status: undefined,
        },
      })
    },
  },

  reducers: {
    registerHandle(state, { payload }) {
      setAuthority('user');
      reloadAuthorized();
      return {
        ...state,
        status: payload.status,
      };
    },
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/user/register') {
          dispatch({
            type: 'resetStatus',
          })
        }
      })
    },
  },
};
