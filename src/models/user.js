import {routerRedux} from 'dva/router';

import {
  query as queryUsers,
  queryCurrent as queryCurrentUser,
  saveUserInfo,
} from '../services/user';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
    userInfo: {},
    menus: [],
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      try {
        const user = yield call(queryCurrentUser);
        yield put({ type: 'save', payload: { currentUser: user } });        
      } catch(error) {
        throw error;
      }
    },

    *saveUserInfo({payload}, {call, put}) {
      try {
        const { userInfo } = payload;
        yield call(saveUserInfo, userInfo );
        yield put(routerRedux.push('/form/contract'));
      } catch (error) {
        throw error
      }
    },
    *fetchUserInfo(_, {call, put}) {
      try {
        const response = yield call(queryCurrentUser);
        yield put ({
          type: 'save',
          payload:{
            userInfo: response,
          },
        })
      } catch(error) {
        throw error
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/form/user-info') {
          dispatch({
            type: 'fetchUserInfo',
          })
        }
      });
    },
  },
};
