import queryString from 'query-string';
import { queryNotices, getUserInfo } from '../services/api';
import {getMenus, saveUser} from '../services/user';

export default {
  namespace: 'global',

  state: {
    collapsed: false,
    notices: [],
    menus: [],
  },

  effects: {
    *fetchNotices(_, { call, put }) {
      const data = yield call(queryNotices);
      yield put({
        type: 'saveNotices',
        payload: data,
      });
      yield put({
        type: 'user/changeNotifyCount',
        payload: data.length,
      });
    },
    *fetchMenus(_, {call, put}){
      const menus = yield call(getMenus)
      yield put ({type: 'save', payload: {menus}});
    },
    *clearNotices({ payload }, { put, select }) {
      yield put({
        type: 'saveClearedNotices',
        payload,
      });
      const count = yield select(state => state.global.notices.length);
      yield put({
        type: 'user/changeNotifyCount',
        payload: count,
      });
    },
    *getAccess_token({payload}, {call, put}) {
      try {
        const userInfo = yield call(getUserInfo, payload);
        console.log('userInfo', userInfo);
        const saveUserResponse = yield call(saveUser, userInfo);
        console.log(saveUserResponse);
        window.location.search = '';
        window.location.href = 'http://app1.wx.maizedna.cn/#/form/contract';
        yield put({type: 'user/fetchCurrent'});
        yield put({type: 'fetchMenus'});
      } catch (error) {
        throw error;
      }
    },
  },

  reducers: {
    save(state, {payload}) {
      return{
        ...state,
        ...payload,
      }
    },
    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },
    saveNotices(state, { payload }) {
      return {
        ...state,
        notices: payload,
      };
    },
    saveClearedNotices(state, { payload }) {
      return {
        ...state,
        notices: state.notices.filter(item => item.type !== payload),
      };
    },
  },

  subscriptions: {
    setup({ history, dispatch }) {
      dispatch({ type: 'fetchMenus' });
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        if (window.location.search !== '') {
          const wxCode = queryString.parse(window.location.search);
          console.log('wxCode', wxCode);
          dispatch({
            type: 'getAccess_token',
            payload: wxCode,
          })
        }
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
