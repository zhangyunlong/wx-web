import { message } from 'antd';
import { matchPath } from 'react-router';
import { routerRedux } from 'dva/router'
import {
  addContract,
  getContractList,
  acceptContract,
  uploadReport,
  getContractInfoById,
} from '../services/contract';
// import {
//   queryCurrent as queryCurrentUser,
// } from '../services/user';

export default {
  namespace: 'contract',

  state: {
    modelVisible: false,
    prevSamples: [],
    samples: [],
    selectedRows: [],
    sample: {},
    editing: false,
    attachments:[],
    reportAttachment: [],
    accept: false,
    arrchiveBarcode: '',
    lookDetail: {},
    samplesInfo: [],
    attachmentsInfo: [],
    reportsInfo: [],
    loading: true,
  },

  effects: {
    *add({payload}, {call, put, select}) {
      try {
        const {attachments, samples} = yield select(state => state.contract);
        const result = yield call(addContract, {
          ...payload,
          attachments,
          samples,
        });
        if (result) {
          yield put({
            type: 'save',
            payload: {
              attachments: [],
              samples: [],
            },
          });
          yield put(routerRedux.push('/form/contract-list'));
  
        } else {
          message.error('保存未成功， 请重新提交');
        }
      } catch (error) {
        throw error
      }
    },
    *fetchContractList(_, { call, put }) {
      try {
        const contractList = yield call(getContractList);
        yield put ({
          type: 'save',
          payload: {
            contractList,
          },
        })
      } catch (error) {
        throw error;
      }
    },
    *acceptContract({ payload }, { call, put }) {
      try {
        const { record } = payload;
        const { arrchiveBarcode } = record.attributes;
        yield call(acceptContract, arrchiveBarcode);
        yield put({
          type: 'fetchContractList',
        })
        yield put ({
          type: 'save',
          payload: {
            accept: true,
          },
        })
      } catch(error) {
        throw error
      }
    },
    *uploadReport({payload}, { call, select }) {
      try {
        const {arrchiveBarcode } = yield select(state => state.contract)
        yield call(uploadReport, {payload, arrchiveBarcode})
        message.success('上传成功');
      } catch(error) {
        throw error;
      }
    },
    *fetchContractById({payload}, { call, put }) {
      try {
        const lookDetail = yield call(getContractInfoById, payload);
        const { samples, attachments, reports} = lookDetail;
        yield put ({
          type: 'save',
          payload: {
            lookDetail,
            loading: false,
            samplesInfo: samples,
            attachmentsInfo: attachments,
            reportsInfo: reports,
          },
        });
      } catch (error) {
        throw error;
      }
    },
    // *fetchRouter(_,{call, put}) {
    //   const user = yield call(queryCurrentUser);
    //   if (user.phone === undefined) {
    //     yield put(routerRedux.push('/form/user-info'));
    //     message.info('请先填写您的信息，再创建委托单！');
    //   }
    // },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/form/contract') {
          dispatch({
            type: 'fetchRouter',
          })
        }
        if (pathname === '/form/contract-list') {
          dispatch({
            type: 'fetchContractList',
          })
        }
        if (pathname === '/form/upload-report') {
          dispatch({
            type: 'fetchContractList',
          })
        }
        const contractInfoMatch = matchPath(pathname, {path: '/form/contract-info/:id'});
        if (contractInfoMatch) {
          const payload = contractInfoMatch.params;
          dispatch({
            type: 'fetchContractById',
            payload,
          })
        }
      })
    },
  },
};
