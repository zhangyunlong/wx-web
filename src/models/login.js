import { routerRedux } from 'dva/router';
import { accountLogin, accountLogout } from '../services/user';
import { reloadAuthorized } from '../utils/Authorized';

export default {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      try {
      yield put({
        type: 'save',
        payload: {
          submitting: true,
        },
      });
      yield call(accountLogin, payload);
      yield put({
        type: 'save',
        payload: {
          status: 'ok',
          submitting: false,
        },
      });
      reloadAuthorized();
      yield put({type: 'user/fetchCurrent'});
      yield put({type: 'global/fetchMenus'});
      } catch (error) {
        yield put({
          type: 'save',
          payload: {
            status: 'error',
            submitting: false,
          },
        });
        throw error;
      }
    },
    *logout(_, { call, put }) {
      try {
        yield call(accountLogout);
        yield put({ type: 'save', payload: { status: undefined } });       
        yield put(routerRedux.push('/user/login'));
      } catch (error) {
        throw error;
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
