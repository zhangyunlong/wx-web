module.exports = {
  name: 'WX-WEB',
  description: 'WX-WEB',
  copyright: '2017 北京华生恒业科技有限公司',
  serverURL: process.env.SERVER_URL || 'http://app1.wx.maizedna.cn:1337/api',
  // downloadURL: process.env.DOWNLOAD_URL || 'http://parse-files.soft.today/download',
  downloadURL: process.env.DOWNLOAD_URL || 'http://files1.wx.maizedna.cn/download',
  appId: process.env.APP_ID || 'wxWeb',
  fileVolumeLimit: 20,
};
