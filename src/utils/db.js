import Promise from 'bluebird';
import _ from 'lodash';

import Parse from './parseServer';


export const insertOne = (model, doc, isConverted = true) => {
  if (!_.isString(model) || !_.isPlainObject(doc) || !_.isBoolean(isConverted)) {
    throw new Error('insertOne 参数不正确');
  }

  const Model = Parse.Object.extend(model);
  const newModel = new Model();

  return newModel
    .save(doc)
    .then((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    });
};

export const insertMany = (model, docs, isConverted = true) => {
  if (!_.isArray(docs)) {
    throw new Error('insertMany 参数不正确');
  }

  return Promise.mapSeries(docs, doc => insertOne(model, doc, isConverted));
};

export const query = (model, params = {}, constraints = []) => {
  if (
    !_.isString(model) ||
    (params && !_.isPlainObject(params)) ||
    (constraints && !_.isArray(constraints))
  ) {
    throw new Error('query 参数不正确');
  }

  const q = new Parse.Query(model);
  Object.keys(params).forEach(k => q.equalTo(k, params[k]));
  constraints.forEach(constraint => Object.keys(constraint).forEach(k => q[k](...constraint[k])));

  return q;
};

export const findOneById = (model, id, isConverted = true) => {
  if (!_.isString(model) || !_.isString(id) || !_.isBoolean(isConverted)) {
    throw new Error('findOneById 参数不正确');
  }

  const q = new Parse.Query(model);

  return q
    .get(id)
    .then((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    });
};

export const findOne = (model, params, isConverted = true) => {
  if (!_.isBoolean(isConverted)) {
    throw new Error('findOne 参数不正确');
  }

  return query(model, params)
    .first()
    .then((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    });
};

export const findMany = (model, params, constraints, isConverted = true) => {
  if (!_.isBoolean(isConverted)) {
    throw new Error('findMany 参数不正确');
  }

  return query(model, params, constraints)
    .find()
    .then(rets => rets.map((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    }));
};

export const updateOneById = (model, id, updates = {}, isConverted = true) => {
  if (!_.isBoolean(isConverted) || (updates && !_.isPlainObject(updates))) {
    throw new Error('updateOneById 参数不正确');
  }

  return findOneById(model, id, false)
    .then((doc) => {
      if (doc) {
        Object.keys(updates).forEach(k => doc.set(k, updates[k]));
        return doc.save();
      } else {
        return doc;
      }
    })
    .then((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    });
};

export const updateOne = (model, params, updates = {}, isConverted = true) => {
  if (!_.isBoolean(isConverted) || (updates && !_.isPlainObject(updates))) {
    throw new Error('updateOne 参数不正确');
  }

  return findOne(model, params, false)
    .then((doc) => {
      if (doc) {
        Object.keys(updates).forEach(k => doc.set(k, updates[k]));
        return doc.save();
      } else {
        return doc;
      }
    })
    .then((ret) => {
      if (ret && isConverted) {
        return ret.toJSON();
      } else {
        return ret;
      }
    });
};

export const updateMany = (model, params, constraints, updates = {}, isConverted = true) => {
  if (!_.isBoolean(isConverted) || (updates && !_.isPlainObject(updates))) {
    throw new Error('updateMany 参数不正确');
  }

  return findMany(model, params, constraints, false)
    .then(docs => Promise.mapSeries(docs, (doc) => {
      Object.keys(updates).forEach(k => doc.set(k, updates[k]));
      return doc.save();
    })
      .then(rets => rets.map((ret) => {
        if (ret && isConverted) {
          return ret.toJSON();
        } else {
          return ret;
        }
      })));
};

export const updateManyById = (model, ids, updates = {}, isConverted = true) => {
  if (!_.isArray(ids)) {
    throw new Error('updateManyById 参数不正确');
  }

  return Promise.mapSeries(ids, id => updateOneById(model, id, updates, isConverted));
};

/*
* // example
* const query = new ParseQuery();
* query.equalTo(..., ...);
* const results = await query.pagination(options); // options default { limit: 10, page: 1 }
*/
export class ParseQuery extends Parse.Query {
  constructor(...args) {
    super(...args);
    this.args = args;
  }

  pagination = (options = {}) => {
    const page = parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
    const limit = parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 10;

    const count = this.count();
    const list = this
      .skip((page - 1) * limit)
      .limit(limit)
      .find()
      .then(rets => rets);

    return Promise.props({
      list,
      pagination: Promise.props({
        current: page,
        pageSize: limit,
        total: count,
      }),
    });
  }
}

export const insertFile = (file, extra, isConverted = true) => {
  if (!(file instanceof File) || (extra && !_.isPlainObject(extra))) {
    throw new Error('insertFile 参数错误');
  }

  const { name, type, size } = file;
  const extname = name.slice(name.lastIndexOf('.'));
  const newFile = new Parse.File('file', file, type);

  return newFile.save()
    .then(ret => insertOne('File', { name, type, size, extname, data: ret, ...extra }, isConverted));
};

export const insertManyFiles = (files, extra, isConverted = true) => {
  if (!_.isArray(files)) {
    throw new Error('insertManyFiles 参数错误');
  }

  return Promise.mapSeries(files, file => insertFile(file, extra, isConverted));
};
