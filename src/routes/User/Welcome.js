import React, { Component } from 'react';
import { connect } from 'dva';
import { Form, Spin } from 'antd';
import styles from './Login.less';

@connect(state => ({
  login: state.login,
}))
@Form.create()
export default class Login extends Component {

  render() {
    return (
      <div className={styles.main}>
        <Spin tip="登录中..." size="large" className={styles.main} />
      </div>
    );
  }
}