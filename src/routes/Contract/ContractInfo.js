import React, { PureComponent } from "react";
import { Card, Table, Button } from 'antd';
import { connect } from 'dva';
import PageHeaderLayout from "../../layouts/PageHeaderLayout";
import DescriptionList from '../../components/DescriptionList';
import { download, key2text } from '../../utils/utils';

const { Description } = DescriptionList;

@connect(state => ({
  ...state.contract,
}))
export default class ContractInfo extends PureComponent{
  render() {
    const { loading, lookDetail, samplesInfo, attachmentsInfo, reportsInfo} = this.props;
    const downloadFile =(record, item)=> {
      const {_url} = record.attributes.data
      const title = item === '1' ? '附件' : '报告';
      download(_url, {filename:`${title}${lookDetail.arrchiveBarcode}`})
    }
    const attachmentColumns = [{
      title: '序号',
      dataIndex: 'num',
      render: (text, record, index) => index + 1,
    },{
      title: '文件名',
      dataIndex: 'attributes.name',
    },{
      title: '操作',
      render:(record)=>(
        <span>
          <Button type="primary" onClick={()=>downloadFile(record, '1')}>下载附件</Button>
        </span>
      ),
    }];
    const sampleColumns = [{
      title: '序号',
      dataIndex: 'num',
      render: (text, record, index) => index + 1,
    },{
      title: '样品名',
      dataIndex: 'attributes.name',
    },{
      title: '样品条码号',
      dataIndex: 'attributes.barcode',
    },{
      title: '样品重量',
      dataIndex: 'attributes.weight',
    }];
    const reportColumns = [{
      title: '序号',
      dataIndex: 'num',
      render: (text, record, index) => index + 1,
    },{
      title: '文件名',
      dataIndex: 'attributes.name',
    }, {
      title: '操作',
      render: (record) => (
        <span>
          <Button type="primary" onClick={()=>downloadFile(record, '2')}>下载报告</Button>
        </span>
      ),
    }];
    return (
      <PageHeaderLayout title={`合同条码号：${lookDetail.arrchiveBarcode}`}>
        <Card title="委托单" bordered={false} loading={loading}>
          <DescriptionList>
            <Description term="种属"> {key2text(lookDetail.species, 'species')} </Description>
            <Description term="检测类型"> {key2text(lookDetail.testType, 'testType')} </Description>
            <Description term="是否加急"> {key2text(lookDetail.isUrgent, 'isUrgent')} </Description>
            <Description term="检测信息">{lookDetail.testContract}</Description>
            <Description term="备注">{lookDetail.remark}</Description>
          </DescriptionList>
        </Card>
        <Card title="附件列表" style={{marginTop: '10px'}} >
          <Table
            columns={attachmentColumns}
            dataSource={attachmentsInfo}
            rowKey={record => record.id}  
            pagination={false}          
          />
        </Card>
        <Card title="样品列表" style={{marginTop: '10px'}} >
          <Table
            columns={sampleColumns}
            dataSource={samplesInfo}
            rowKey={record => record.id}
            pagination={false}                                
          />
        </Card>
        <Card
          title="报告列表"
          style={{marginTop: '10px'}} 
        >
          <Table
            columns={reportColumns}
            dataSource={reportsInfo}
            rowKey={record => record.id}
            pagination={false}                     
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}