import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Input,
  Button,
  Card,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import FooterToolbar from '../../components/FooterToolbar';


const FormItem = Form.Item;


@connect(state => ({
  ...state.user,
}))
@Form.create()
export default class BasicForms extends PureComponent {
  render() {
    const { form, dispatch, userInfo } = this.props;
    const { getFieldDecorator, validateFields, resetFields } = form;
    const handleSubmit = () => {
      validateFields((err, values) => {
        if (err) {
          return;
        }
        dispatch({
          type: 'user/saveUserInfo',
          payload: {
            userInfo: values,
          },
        })
      })
      resetFields();
    }
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };

    return (
      <PageHeaderLayout
        title="完善个人信息"
        content="完善相关信息"
      >
        <Card bordered={false}>
          <Form style={{ marginTop: 8 }}>
            <FormItem {...formItemLayout} label="单位名称">
              {getFieldDecorator('unitName',{
                rules:[{required:true, message: '单位名称'}],
                initialValue: userInfo.unitName,             
              })(
                <Input placeholder="请输入单位名称" />                
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="联系人">
              {getFieldDecorator('linkName',{
                rules:[{required:true, message: '联系人'}],
                initialValue: userInfo.linkName,                            
              })(
                <Input placeholder="请输入联系人姓名" />                
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="联系电话">
              {getFieldDecorator('phone', {
                rules:[{required:true, message: '联系电话'}],
                initialValue: userInfo.phone,                                
              })(
                <Input placeholder="请输入联系电话" />                
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="地址">
              {getFieldDecorator('addr', {
                rules:[{required:true, message: '地址'}],
                initialValue: userInfo.addr,                              
              })(
                <Input placeholder="请输入地址" />                
              )}
            </FormItem>
          </Form>
        </Card>
        <FooterToolbar>
          <Button type="primary" onClick={()=>handleSubmit()}>保存</Button>
        </FooterToolbar>
      </PageHeaderLayout>
    );
  }
}
