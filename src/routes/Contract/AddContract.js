import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Input,
  Select,
  Button,
  Card,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import FooterToolbar from '../../components/FooterToolbar';
import SampleList from './SampleList';
import Attachment from './Attachment';

const FormItem = Form.Item;
const { Option } = Select;
const { TextArea } = Input;


@connect(state => ({
  ...state.contract,
}))
@Form.create()

export default class AddContract extends PureComponent {
  render() {
    const { form, ...restProps } = this.props
    const { getFieldDecorator, validateFields, resetFields } = form;
    const { dispatch } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };
    const submitContract = () => {
      validateFields((err, value) => {
        if (err) {
          return;
        }
        dispatch({
          type: 'contract/add',
          payload: {
            contract: value,
          },
        })
      });
      resetFields();
    }
    return (
      <PageHeaderLayout
        title="新建委托单"
      >
        <Card bordered={false} title="检验信息">
          <Form>
            <FormItem {...formItemLayout} label="种属">
              {getFieldDecorator('species', {
                rules: [
                  {
                    required: true,
                    message: '种属',
                  },
                ],
                initialValue: '0',
              })(
                <Select placeholder="请选择种属">
                  <Option value="0">玉米</Option>
                  {/* <Option value="1">小麦</Option> */}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="检测类型">
              {getFieldDecorator('testType', {
                rules: [
                  {
                    required: true,
                    message: '请选择检测类型',
                  },
                ],
                initialValue: ['0'],
              })(
                <Select
                  mode="multiple"
                  style={{ width: '100%' }}
                  placeholder="Please select"
                >
                  <Option value="0">真实性</Option>
                  <Option value="1">纯度</Option>
                  <Option value="2">转基因</Option>
                  <Option value="3">一致性</Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="是否加急">
              {getFieldDecorator('isUrgent', {
                rules: [
                  {
                    required: true,
                    message: '是否加急',
                  },
                ],
              })(
                <Select placeholder="请选择是否加急">
                  <Option value="0">是</Option>
                  <Option value="1">否</Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="检验信息">
              {getFieldDecorator('testContract', {
                rules: [
                  {
                    required: true,
                    message: '请输入检验信息',
                  },
                ],
              })(
                <TextArea
                  style={{ minHeight: 32 }}
                  placeholder="检验信息"
                  rows={4}
                />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="备注">
              {getFieldDecorator('remark', {
                rules: [
                  {
                    required: true,
                    message: '请输入备注',
                  },
                ],
              })(
                <TextArea style={{ minHeight: 32 }} placeholder="请输入备注" rows={4} />
              )}
            </FormItem>
          </Form>
        </Card>
        <Attachment {...restProps} />
        <SampleList {...restProps} />
        <FooterToolbar>
          <Button type="primary" onClick={submitContract}>提交</Button>
        </FooterToolbar>
      </PageHeaderLayout>
    );
  }
}
