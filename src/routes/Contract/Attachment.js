import React, { PureComponent } from 'react';
import {
  Card,
  Table,
  Button,
  Form,
  Upload,
} from 'antd';


@Form.create()
export default class Attachment extends PureComponent {
  render() {
    const { attachments, dispatch } = this.props;
    const handleDelete = (record) => {
      const newAttachments = attachments.filter(item =>item.uid !== record.uid);
      dispatch({
        type: 'contract/save',
        payload: {
          attachments: newAttachments,
        },
      })
    }
    const columns = [{
      title: '编号',
      dataIndex: 'num',
      key: 'num',
      render: (text, record, index) => index+1,
    }, {
      title: '名称',
      dataIndex: 'name',
    }, {
      title: '操作',
      render: (text, record) => (
        <span style={{ color: 'rgb(0, 134, 255)', cursor: 'pointer' }} onClick={() =>handleDelete(record)}>
          删除
        </span>
      ),
    }];
    const customRequest = ({ file }) => {
      if (file) {
        const newAttachments = [file, ...attachments];
        dispatch({
          type: 'contract/save',
          payload: { attachments: newAttachments },
        });
      }
    };
    return(
      <Card
        title="附件信息"
        extra={
          <div>
            <Upload
              name='file'
              multiple={false}
              showUploadList={false}
              customRequest={customRequest}
            >
              <Button type="primary">上传附件</Button>
            </Upload>
          </div>
        }
      >
        <Table 
          columns={columns}
          dataSource={attachments}
          rowKey={record => record.uid}
          pagination={false}
        />
      </Card>
    );
  }
}
