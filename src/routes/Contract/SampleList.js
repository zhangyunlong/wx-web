import React, { PureComponent } from 'react';
import {
  Card,
  Table,
  Button,
  Modal,
  Form,
  Input,
  Select,
} from 'antd';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select

@Form.create()
export default class SampleList extends PureComponent {
  render() {
    const { form, modelVisible, dispatch, samples, selectedRows, sample, editing } = this.props;
    const { getFieldDecorator, resetFields, validateFields } = form;
    const modalFormLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };
    const newSample = () => {
      dispatch({
        type: 'contract/save',
        payload:{
          modelVisible: true,
          sample: [],
          editing: false,
        },
      })
    }
    const handleEdit = (record) => {
      dispatch({
        type: 'contract/save',
        payload: {
          sample: record,
          modelVisible: true,
          editing: true,
        },
      })
    }
    const handelDelete = () => {
      if (selectedRows.length === 0) {
        return;
      }
      Modal.confirm({
        title: '确认删除',
        onOk() {
          const updataSample = samples.filter(item => !selectedRows.includes(item.barcode));
          dispatch({
            type: 'contract/save',
            payload: {
              samples: updataSample,
              selectedRows: [],
            },
          })
        },
      })

    }
    const handleModalClose = () => {
      dispatch({
        type: 'contract/save',
        payload:{
          modelVisible: false,
        },
      });
      resetFields();
    }
    const handleSubmit = () => {
      const prevSamples = samples;
      validateFields((err, value) => {
        if (err) {
          return;
        }
        let sampleInfo = [];
        if (editing) {
          sampleInfo = samples.map(item=>(item.barcode === sample.barcode) ? value : item);
        } else {
          sampleInfo = [value, ...prevSamples];
        }
        dispatch({
          type: 'contract/save',
          payload: {
            modelVisible: false,
            samples: sampleInfo,
          },
        })
        resetFields();
      });
    }
    const columns = [{
      title: '编号',
      dataIndex: 'num',
      key: 'num',
      render: (text, record, index) => index+1,
    },{
      title: '样品条码',
      dataIndex: 'barcode',
    }, {
      title: '名称',
      dataIndex: 'name',
    }, {
      title: '重量',
      dataIndex: 'weight',
    }, {
      title: '单位',
      dataIndex: 'unit',
    }, {
      title: '备注',
      dataIndex: 'remark',
    }, {
      title: '操作',
      render: (text, record) => (
        <span style={{ color: 'rgb(0, 134, 255)', cursor: 'pointer' }} onClick={() =>handleEdit(record)}>
          编辑
        </span>
      ),
    }];
    
    return(
      <Card
        title="样品信息"
        extra={
          <div>
            <Button style={{ marginRight: '5px' }} onClick={newSample}>新建</Button>
            <Button onClick={handelDelete}>删除</Button>
          </div>
        }
      >
        <Table 
          columns={columns}
          dataSource={samples}
          rowKey={record => record.barcode}
          pagination={false}
          rowSelection={{
            onChange(selectedRowKeys){
              dispatch({
                type: 'contract/save',
                payload: {
                  selectedRows: selectedRowKeys,
                },
              })
            },
          }}
        />
        <Modal
          title="样品信息"
          visible={modelVisible}
          okText="保存"
          cancelText="关闭"
          maskClosable={false}
          onOk={handleSubmit}
          onCancel={handleModalClose}
        >
          <Form>
            <FormItem {...modalFormLayout} label="条码号">
              {getFieldDecorator('barcode', {
                rules: [
                  { required: true, message: '请输入条码号' },
                ],
                initialValue: sample.barcode,
              })(
                <Input
                  placeholder="请输入条码号"
                />
              )}
            </FormItem>
            <FormItem {...modalFormLayout} label="名称">
              {getFieldDecorator('name', {
                rules: [{ required: true, message: '请输入名称' }],
                initialValue: sample.name,
              })(
                <Input placeholder="请输入名称" />
              )}
            </FormItem>
            <FormItem {...modalFormLayout} label="送样量">
              {getFieldDecorator('weight', {
                rules: [{ required: true, message: '请输入送样量' }],
                initialValue: sample.weight,
              })(
                <Input placeholder="请输入送样量" style={{ width: '100%' }} />
              )}
            </FormItem>
            <FormItem {...modalFormLayout} label="单位">
              {getFieldDecorator('unit', {
                initialValue: sample.unit,
              })(
                <Select>
                  <Option value='g'>克</Option>
                  <Option value='kg'>千克</Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...modalFormLayout} label="备注">
              {getFieldDecorator('remark', {
                initialValue: sample.remark,
              })(
                <TextArea placeholder="备注" style={{ resize: 'none' }} rows={4} />
              )}
            </FormItem>
          </Form>
        </Modal>
      </Card>
    );
  }
}
