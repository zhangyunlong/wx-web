import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Table,
  Card,
  Form,
  Button,
  Upload,
} from 'antd';
import moment from 'moment';

import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { key2text } from '../../utils/utils';

@connect(state => ({
  ...state.contract,
}))
@Form.create()

export default class ContractList extends PureComponent{
  render() {
    const { contractList, dispatch } = this.props;
    const customRequest = ({file}) => {
      if (file) {
        dispatch({
          type: 'contract/uploadReport',
          payload: {
            reportAttachment: file,
          },
        })
      }
    }
    const getBarcode = (record) => {
      const { arrchiveBarcode } = record.attributes;
      dispatch({
        type: 'contract/save',
        payload: {
          arrchiveBarcode,
        },
      })
    }
    const acceptContract = (record) => {
      dispatch({
        type: 'contract/acceptContract',
        payload: {
          record,
        },
      })
    }
    const columns = [{
      title: '编号',
      dataIndex: 'num',
      render: (text, record, index) => index+1,
    }, {
      title: '合同编码',
      dataIndex: 'attributes.arrchiveBarcode',
    }, {
      title: '创建日期',
      dataIndex: 'createAt',
      render: (text) => moment(text).format('YYYY-MM-DD'),
    }, {
      title: '检测内容',
      dataIndex: 'attributes.testContract',
    }, {
      title: '合同状态',
      dataIndex: 'attributes.status',
      render: text=>key2text(text, 'contractStatus'),
    }, {
      title: '操作',
      render: (text, record) => (
        <span style={{ color: 'rgb(0, 134, 255)', cursor: 'pointer' }}>
          <Upload
            name={record.attributes.arrchiveBarcode}
            multiple={false}
            showUploadList={false}
            beforeUpload={()=>getBarcode(record)}
            customRequest={customRequest}
          >
            <Button type="primary" size="small">上传报告</Button>
          </Upload>
          {record.attributes.status === '1' &&(
            <Button type="primary" size="small" onClick={()=>acceptContract(record)} style={{marginLeft: '10px'}}>接受委托单</Button>
          )}
        </span>
      ),
    }]
    return (
      <PageHeaderLayout
        title="上传报告"
      >
        <Card>
          <Table
            columns={columns}
            dataSource={contractList}
            rowKey={record => record.id}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}