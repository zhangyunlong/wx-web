import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Table,
  Card,
  Form,
  Button,
  Icon,
} from 'antd';
import moment from 'moment';

import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { key2text } from '../../utils/utils';

@connect(state => ({
  ...state.contract,
}))
@Form.create()

export default class ContractList extends PureComponent{
  render() {
    const { contractList, dispatch } = this.props;
    const lookContrct = (record) => {
      const { arrchiveBarcode } = record.attributes;
      dispatch(routerRedux.push(`/form/contract-info/${arrchiveBarcode}`))
    }
    const columns = [{
      title: '编号',
      dataIndex: 'num',
      render: (text, record, index) => index+1,
    }, {
      title: '合同编码',
      dataIndex: 'attributes.arrchiveBarcode',
    }, {
      title: '创建日期',
      dataIndex: 'createAt',
      render: (text) => moment(text).format('YYYY-MM-DD'),
    }, {
      title: '检测内容',
      dataIndex: 'attributes.testContract',
    }, {
      title: '合同状态',
      dataIndex: 'attributes.status',
      render: text=>key2text(text, 'contractStatus'),      
    }, {
      title: '操作',
      render: (record) => (
        <span style={{ color: 'rgb(0, 134, 255)', cursor: 'pointer' }}>
          <Button type="primary" size="small" onClick={()=>lookContrct(record)}>
            <Icon type="eye" />
            查看 
          </Button>
        </span>
      ),
    }]
    return (
      <PageHeaderLayout
        title="合同列表"
      >
        <Card>
          <Table
            columns={columns}
            dataSource={contractList}
            rowKey={record => record.id}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}