import Promise from 'bluebird';
import Parse from '../utils/parseServer';

import {
  insertOne,
  insertFile,
} from '../utils/db';
import {
  generateBarcode,
} from '../utils/utils';

export const addContract = async (data) => {
  const {attachments, samples, contract} = data;
  const barcodeObj = await generateBarcode();
  const { code, year, typeCode, serialNumber } = barcodeObj.toJSON();
  const barcode = `${code}${year}${typeCode}${serialNumber}`;

  const newAttachmentItems = Promise.mapSeries(
    attachments,
    attachment => insertFile(attachment, { deleted: false }, false),
  );
  const newSampleItems = Promise.mapSeries(
    samples,
    sample => insertOne('Sample', { ...sample, deleted: false }, false),
  );
  const obj = {
    newSamples: newSampleItems,
    newAttachments: newAttachmentItems,
  };

  return Promise.props(obj).then((ret) => {
    const {
      newSamples,
      newAttachments,
    } = ret;

    const Contract = Parse.Object.extend('Contract');
    const newContract = new Contract();

    if (newSamples.length > 0) {
      const sampleRelation = newContract.relation('samples');
      sampleRelation.add(newSamples);
    }

    if (newAttachments.length > 0) {
      const attachmentRelation = newContract.relation('attachments');
      attachmentRelation.add(newAttachments);
    }

    const compileUser = Parse.User.current();
    return newContract.save({
      ...contract,
      compileUser,
      deleted: false,
      arrchiveBarcode: barcode,
      status: '1',
    });
  });
}
export const getContractList = async () => {
  const currentUser = Parse.User.current();
  const { type } = currentUser.attributes;
  if (type === 'Guest') {
    const q = new Parse.Query('Contract');
    q.equalTo('compileUser', Parse.User.current());
    const data = await q.descending('updatedAt').find();
    return data;
  } else if (type === 'Admin') {
    const q = new Parse.Query('Contract');
    const data = await q.descending('updatedAt').find();
    return data;
  }
}

export const acceptContract = async (id) => {
  const q = new Parse.Query('Contract');
  q.equalTo('arrchiveBarcode', id);
  const data = await q.first();
  data.set('status', '2');
  data.save();
  return data;
}

export const uploadReport = async (data) => {
  const { arrchiveBarcode, payload } = data;
  const { reportAttachment } = payload;
  const reportAttachments = [reportAttachment]
  const newAttachmentItems = Promise.mapSeries(
    reportAttachments,
    report => insertFile(report, { deleted: false }, false),
  );

  const obj = {
    newAttachments: newAttachmentItems,
  };

  const q = new Parse.Query('Contract');
  q.equalTo('arrchiveBarcode', arrchiveBarcode);
  const contractItem = await q.first();

  return Promise.props(obj).then((ret) => {
    const {
      newAttachments,
    } = ret;

    if (newAttachments.length > 0) {
      const attachmentRelation = contractItem.relation('reportAttachment');
      attachmentRelation.add(newAttachments);
    }

    return contractItem.save();
  });

}

export const getContractInfoById = async (payload) => {
  const { id } = payload;
  const q = new Parse.Query('Contract');
  q.equalTo('arrchiveBarcode', id);
  const data = await q.first();
  const detail = data.toJSON();
  if (!data) {
    return 'error';
  }
  // 查询样品relation
  const samplesRelation = data.relation('samples');
  const samplesRelationQuery = samplesRelation.query();
  const samplesInfo = await samplesRelationQuery.find();
  // 查询附件relation
  const attachmentsRelation = data.relation('attachments');
  const attachmentsRelationQuery = attachmentsRelation.query();
  const attachmentsInfo = await attachmentsRelationQuery.find();
  // 查询报告relation
  const reportsRelation = data.relation('reportAttachment');
  const reportsRelationQuery = reportsRelation.query();
  const reportsInfo = await reportsRelationQuery.find();

  detail.samples = samplesInfo;
  detail.attachments = attachmentsInfo;
  detail.reports = reportsInfo;

  return detail;
}