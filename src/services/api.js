import { stringify } from 'qs';
import moment from 'moment';
import request from '../utils/request';

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}


export async function getUserInfo(params) {
  try {
    const { code } = params;
    const currentTime = moment();
    const storage =window.localStorage;
    const expireTime = storage.getItem('expireTime');
    // token 未过期
    if (expireTime>=currentTime) {
      const userInfo = await request(`https://api.weixin.qq.com/sns/userinfo?access_token=${storage.getItem('access_token')}&openid=${storage.getItem('openid')}&lang=zh_CN`, {
        method: 'GET',
      });
      return userInfo;
    }else {
      // 无token或token过期
      // 获取token
      // 测试账号
      // const getToken = await request(`https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx1bcc1894111e794b&secret=4cbeedd307a97559b448b5d374c7d4cf&code=${code}&grant_type=authorization_code`, {
      //   method: 'GET',
      // });
      // 服务号
      const getToken = await request(`https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx53224f4db7431147&secret=a48f379b0143e540df75ffbb07db88e2&code=${code}&grant_type=authorization_code`, {
        method: 'GET',
      });
      const setStorage =(k,v)=>{
        if (k === 'expires_in') {
         storage.setItem(k, currentTime+v);
        }else{
         storage.setItem(k, v);
        }
      }
      Object.keys(getToken).forEach(k=>setStorage(k, getToken[k]));

      const { access_token: accessToken, openid } = getToken;
      const userInfo = await request(`https://api.weixin.qq.com/sns/userinfo?access_token=${accessToken}&openid=${openid}&lang=zh_CN`, {
        method: 'GET',
      });
      return userInfo
    }
  } catch(error) {
    throw error;
  }
}

export async function getFile(params) {
  console.log('params', params);
  const { _url } = params;
  return request(_url, {
    method: 'GET',
  })
}
