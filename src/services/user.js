import Promise from 'bluebird';
import Parse from '../utils/parseServer';
import request from '../utils/request';
import { getMenuData } from '../common/menu';


export async function query() {
  return request('/api/users');
}

export const fakeRegister = async (payload) => {
  const queryRole = new Parse.Query(Parse.Role);
  queryRole.equalTo('name', 'Guest')
  const role = await queryRole.first();
  const user = new Parse.User();
  Object.keys(payload).forEach(k=>user.set(k,payload[k]));
  const saveUser = user.save();
  const obj = {
    newUser: saveUser,
  }
  return Promise.props(obj).then((ret)=>{
    const {newUser} = ret;
    const userRelation = role.relation('users');
    userRelation.add(newUser);
    return role.save()
  })
}

export const saveUser = async (payload) => {
  const {openid} = payload
  const username = openid;
  const password = openid;

  // 先根据openid查询用户表 如有直接登陆；如果没有先存储后登陆，
  const q = new Parse.Query('User');
  q.equalTo('openid', openid);
  const existUser = await q.first();

  if (!existUser) {
    // 存储新用
    if (openid === 'owYzs06i1u5CnCa9l3TqNiR9c_OA') {
      const queryRole = new Parse.Query(Parse.Role);
      queryRole.equalTo('name', 'Administrator')
      const role = await queryRole.first();
      const user = new Parse.User();
      Object.keys(payload).forEach(k=>user.set(k,payload[k]));
      user.set('password', password);
      user.set('username', username);
      user.set('type', 'Admin');
      const userInfo = user.save();
      const obj = {
        newUser: userInfo,
      }
      return Promise.props(obj).then((ret)=>{
        const {newUser} = ret;
        const userRelation = role.relation('users');
        userRelation.add(newUser);
        return role.save()
      })
    } else {
      const queryRole = new Parse.Query(Parse.Role);
      queryRole.equalTo('name', 'Guest')
      const role = await queryRole.first();
      const user = new Parse.User();
      Object.keys(payload).forEach(k=>user.set(k,payload[k]));
      user.set('password', password);
      user.set('username', username);
      user.set('type', 'Guest');
      const userInfo = user.save();
      const obj = {
        newUser: userInfo,
      }
      return Promise.props(obj).then((ret)=>{
        const {newUser} = ret;
        const userRelation = role.relation('users');
        userRelation.add(newUser);
        return role.save()
      })
    }
  }
  // 登陆
  const LoginUser = await Parse.User.logIn(username, password);
  return LoginUser;
}

export const accountLogin = async (payload) => {
  try {
    const { username, password} = payload;
    const user = await Parse.User.logIn(username, password);
    return user
  } catch (error) {
    throw error;
  }
}

export async function queryCurrent() {
  const user = await Parse.User.current();
  if (user) {
    const {
      objectId: id,
      username,
      email,
      phone,
      nickname,
      status,
      code,
      gender,
      addr,
      remark,
      linkName,
      unitName,
      type,
      city,
      sex,
      headimgurl,
      province,
    } = user.toJSON();
    return {
      id,
      code,
      username,
      email,
      phone,
      nickname,
      status,
      gender,
      addr,
      remark,
      linkName,
      unitName,
      type,
      city,
      sex,
      headimgurl,
      province,
    };
  }
  return {};
}

export async function accountLogout() {
  try {
    await Parse.User.logOut();
  } catch (error) {
    throw error;
  }
}
export async function saveUserInfo(payload) {
  const user = await Parse.User.current();
  Object.keys(payload).forEach(k =>
    user.set(k,payload[k])
  ); 
  return user.save();
}


export const getMenus = () => {
  const user = Parse.User.current();
  if (user) {
    const { type } = user.attributes;
    const menuData = getMenuData();
    const menus = menuData.filter((item)=>{return item.name === '表单页'});
    if (type === 'Guest') {
      const { children } = menus[0];
      const result = children.filter((item)=>{return item.name !== '上传报告'})
      menus[0].children = result;
      return menus;
    } else {
      return menus;
    }
  }
  return [];
}