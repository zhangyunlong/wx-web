const async = require('async');
const Parse = require('parse/node');

Parse.initialize(process.env.APP_ID || 'wxWeb', '', process.env.MASTER_KEY || 'key-wxWeb');
Parse.serverURL = process.env.SERVER_URL || 'http://localhost:1337/api';

const admins = [
  {
    email: 'admin@domain.com',
    password: '123456',
    username: 'admin',
    nickname: '管理员',
    phone: '15649845188',
    status: 'enabled',
    sort: 1,
    code: 'WXWEB00001',
    gender: 'male',
    abbr: 'GUGANLIYUAN',
    emailConfirm: 'admin@domain.com',
    deleted: false,
    type: 'Admin',
  },
];

function createAdmin(data, done) {
  const User = Parse.Object.extend('User');
  const user = new User();
  Object.keys(data).forEach(k => user.set(k, data[k]));
  user.save().then(() => {
    done();
  }).catch((err) => {
    done(err);
  });
}

module.exports = (done) => {
  async.each(admins, createAdmin, done);
};
