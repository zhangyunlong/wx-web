const Parse = require('parse/node');
const Promise = require('bluebird');

Parse.initialize(process.env.APP_ID || 'wxWeb', '', process.env.MASTER_KEY || 'key-wxWeb');
Parse.serverURL = process.env.SERVER_URL || 'http://localhost:1337/api';

const roles = [{
    name: 'Guest',
    displayName: '用户',
  }];

module.exports = (done) => {
  const roleACL = new Parse.ACL();
  roleACL.setPublicReadAccess(true);
  roleACL.setPublicWriteAccess(true);

  return Promise.mapSeries(roles, (item) => {
    const { name, displayName } = item;
    const role = new Parse.Role(name, roleACL);
    role.set('displayName', displayName);
    role.set('status', 'enabled');
    return role.save();
  })
    .then(() => done())
    .catch(e => done(e));
};
