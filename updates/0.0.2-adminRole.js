const Parse = require('parse/node');

Parse.initialize(process.env.APP_ID || 'wxWeb', '', process.env.MASTER_KEY || 'key-wxWeb');
Parse.serverURL = process.env.SERVER_URL || 'http://localhost:1337/api';

module.exports = (done) => {
  const roleACL = new Parse.ACL();
  roleACL.setPublicReadAccess(true);
  roleACL.setPublicWriteAccess(true);
  const role = new Parse.Role('Administrator', roleACL);
  role.set('status', 'enabled');
  role.set('displayName', '超级管理员');
  const queryUser = new Parse.Query(Parse.User);
  queryUser.equalTo('username', 'admin');

  return queryUser.first()
    .then(user => role.relation('users').add(user).save())
    .then(() => {
      done();
    });
};
