require('dotenv').config();
const express = require('express');
const { ParseServer } = require('parse-server');
const ParseDashboard = require('parse-dashboard');
const path = require('path');
const bodyParser = require('body-parser');
const request = require('request');
const updates = require('./updates');


// 139.198.126.31
// http://wx.maizedna.com.cn/
const Console = console;
const databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  Console.log('DATABASE_URI not specified, falling back to localhost.');
}

const api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://192.168.100.2:27017/wxWeb-db',
  appId: process.env.APP_ID || 'wxWeb',
  masterKey: process.env.MASTER_KEY || 'key-wxWeb',
  serverURL: process.env.SERVER_URL || 'http://app1.wx.maizedna.cn:1337/api',
  maxUploadSize: process.env.MAX_UPLOAD_SIZE || '20mb',
});

const dashboard = new ParseDashboard({
  apps: [
    {
      serverURL: process.env.SERVER_URL || 'http://app1.wx.maizedna.cn:1337/api',
      appId: process.env.APP_ID || 'wxWeb',
      masterKey: process.env.MASTER_KEY || 'key-wxWeb',
      appName: 'wxWeb',
    },
  ],
});

const app = express();
app.use(bodyParser.json());
// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
const mountPath = process.env.PARSE_MOUNT || '/api';
app.use(mountPath, api);

app.use('/dashboard', dashboard);

app.use('/download', (req, res) => {
  const { url, filename } = req.query;
  const filenameByUrl = url.match(/([^/]+)(?=\.\w+$)/)[0];
  const fileExt = url.match(/([^.]+$)/)[0];
  res.setHeader(
    'content-disposition',
    `attachment; filename=${encodeURIComponent(`${filename || filenameByUrl}.${fileExt}`)}`
  );
  req.pipe(request(url)).pipe(res);
});

// Parse Server plays nicely with the rest of your web routes
app.get('/', (req, res) => {
  res.status(200).send('Hello World ~');
});

updates.apply((err) => {
  if (err) return Console.error(err);
});
const port = process.env.PORT || 1337;
const httpServer = require('http').createServer(app);

httpServer.listen(port, () => {
  Console.log(`backend running on port ${port}.`);
});
